import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
} from 'react-native';
import Navbar from './src/components/Navbar';
import AddTodo from './src/components/AddTodo';
import Todo from './src/components/Todo';

const initial = [
  {id: '1603199049640', isDone: false, title: 'dsadsad'},
  {
    id: '1603199051340',
    isDone: false,
    title: 'dsadsadsa',
  },
  {id: '1603199053248', isDone: false, title: '1212131'},
];

const App: () => React$Node = () => {
  const [todos, setTodos] = useState(initial);
  const addTodo = (title) => {
    setTodos((prev) => [
      ...prev,
      {
        title,
        isDone: false,
        id: Date.now().toString(),
      },
    ]);
  };
  const removeTodo = (id) => {
    setTodos((prev) => prev.filter((item) => item.id !== id));
  };

  const finishTodo = (id) => {
    let index = todos.findIndex((el) => el.id === id);
    setTodos((prev) =>
      prev.map((el) => {
        if (id === el.id) {
          el.isDone = !el.isDone;
        }
        return el;
      }),
    );
  };

  return (
    <>
      <Navbar />
      <View style={styles.wrapper}>
        <AddTodo addTodo={addTodo} />
        <FlatList
          data={todos}
          renderItem={({item}) => (
            <Todo finishTodo={finishTodo} removeTodo={removeTodo} item={item} />
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {},
  text: {
    fontSize: 30,
  },
});

export default App;
