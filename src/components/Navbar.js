import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Navbar = () => {
  return (
    <>
      <View style={styles.navbar}>
        <Text style={{fontSize: 25}}>Navbar</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: '#3949ab',
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Navbar;
