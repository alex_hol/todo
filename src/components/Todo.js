import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

function Todo({item, removeTodo, finishTodo}) {
  const onRemoveHandler = () => {
    removeTodo(item.id);
  };
  const onTodoDone = () => {
    finishTodo(item.id);
  };

  return (
    <TouchableOpacity
      style={styles.todo}
      onPress={onTodoDone}
      onLongPress={onRemoveHandler}>
      <CheckBox
        disabled={false}
        value={item.isDone}
        onValueChange={onTodoDone}
      />
      <Text
        style={
          item.isDone
            ? {...styles.title, textDecorationLine: 'line-through'}
            : styles.title
        }>
        {item.title}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    borderColor: 'grey',
    margin: 5,
  },
  title: {
    fontSize: 20,
    padding: 10,
  },
});

export default Todo;
