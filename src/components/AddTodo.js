import React, {useState} from 'react';
import {View, TextInput, StyleSheet, Button} from 'react-native';

function AddTodo({addTodo}) {
  const [value, setValue] = useState('');
  const addHandler = () => {
    if (value.trim()) {
      addTodo(value);
      setValue('');
    }
  };
  return (
    <View style={styles.container}>
      <TextInput
        value={value}
        style={styles.input}
        placeholder="Add todo"
        onChangeText={setValue}
      />
      <Button onPress={addHandler} title="Add" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  input: {
    borderBottomWidth: 1,
    width: '70%',
    fontSize: 18,
  },
});

export default AddTodo;
